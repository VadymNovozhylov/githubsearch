package com.vadymnovozhylov.githubsearch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.repo_row.view.*

class ReposAdapter(private val repos: List<Repo>) : RecyclerView.Adapter<ReposAdapter.RepoViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.repo_row, parent, false)
        return RepoViewHolder(view)
    }

    override fun getItemCount() = repos.size

    override fun onBindViewHolder(holderRepo: RepoViewHolder, position: Int) {
        holderRepo.bind(repos[position])
    }

    class RepoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val repoName: TextView = itemView.repoName
        private val repoDescription: TextView = itemView.repoDescription
        private val repoUrl: TextView = itemView.repoUrl
        private val repoPicture: ImageView = itemView.repoPicture

        fun bind(repo: Repo) {
            repoName.text = repo.name
            repoDescription.text = repo.description
            repoUrl.text = repo.html_url
            Picasso.with(itemView.context).load(repo.owner.avatar_url).into(repoPicture)
        }
    }
}


