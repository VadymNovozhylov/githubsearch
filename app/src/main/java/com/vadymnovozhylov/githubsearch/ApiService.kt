package com.vadymnovozhylov.githubsearch

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/search/repositories")
    fun searchForName(
        @Query("q") query: String
    ): Call<Item>

    @GET("/repositories")
    fun fetchAllNames(): Call<List<Repo>>
}