package com.vadymnovozhylov.githubsearch

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private lateinit var apiService: ApiService
    private lateinit var retrofit: Retrofit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initWebService()
        firstLoad()
        initListener()

    }

    private fun firstLoad(){
        apiService.fetchAllNames().enqueue(object : Callback<List<Repo>> {

            override fun onResponse(call: Call<List<Repo>>, response: Response<List<Repo>>) {
                showData(response.body()!!)
            }

            override fun onFailure(call: Call<List<Repo>>, t: Throwable) {
                println("onFailure")
            }

        })
    }

    private fun initListener() {
        searchButton.setOnClickListener {
            val searchText = textInput.text
            queryRepo(searchText.toString())
        }
    }

    private fun initWebService() {
        retrofit = Retrofit.Builder()
            .baseUrl("https://api.github.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        apiService = retrofit.create(ApiService::class.java)
    }

    private fun queryRepo(query: String) {
        apiService.searchForName(query).enqueue(object : Callback<Item> {

            override fun onResponse(call: Call<Item>, response: Response<Item>) {
                showData(response.body()!!.list)
            }

            override fun onFailure(call: Call<Item>, t: Throwable) {
                println("onFailure")
            }

        })
    }

    private fun showData(repos: List<Repo>) {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = ReposAdapter(repos)
        }
    }
}
