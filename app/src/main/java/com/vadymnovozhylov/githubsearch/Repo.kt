package com.vadymnovozhylov.githubsearch

import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("items")
    val list: List<Repo>
)

data class Repo(
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("html_url")
    val html_url: String,
    @SerializedName("owner")
    val owner: Owner
)

data class Owner(
    @SerializedName("avatar_url")
    val avatar_url: String
)